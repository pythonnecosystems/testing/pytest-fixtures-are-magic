# pytest fixtures are magic

## 테스트 작성의 주요 문제
자동화된 테스트를 작성할 때 발생하는 문제 중 하나는 테스트의 단계를 설정하는 것이다. 테스트하려는 항목을 실행하기 전에 필요한 특정 데이터를 생성해야 한다.

지루한 작업이지만 반드시 그럴 필요는 없다!

이것이 왜 문제가 될까? 많은 시스템에서 봄에 나뭇잎이 초록색으로 변하는지 확인하기 위해 나무의 모든 가지, 뿌리, 땅, 하늘을 생성하는 것은 번거로운 일이다.

예를 들어, Github에서 일하면서 공개 리포지토리의 이슈 댓글에 욕설을 대체하는 기능을 추가하는 임무를 맡았다고 가정해 보자.

- 조직에 소유자가 필요하므로 사용자 만들기
- 이 리포지토리를 소유할 조직을 만든다.
- 주요 기능을 테스트하기 위해 공개 리포지토리를 만든다.
- 비공개 리포지토리를 만들어 해당 댓글을 건드리지 않도록 한다.
- 이슈를 만든다.

그런 **다음**, 욕설이 포함되거나 포함되지 않은 댓글을 작성하여 작업 중인 기능을 테스트할 수 있다. 테스트하기 전에 만들어야 할 것들이 참 많다. 사람들이 게을러져서 기능을 처음부터 끝까지 테스트하지 않는 것은 놀라운 일이 아니다.

정답은 테스트를 피하는 것이 아니라 애초에 데이터를 훨씬 쉽게 생성할 수 있도록 하는 것이다.

pytest 도구의 마법은 테스트에 삽입하여 사용할 수 있도록 하는 방법과 그 구성 가능성이다. 잘 활용하면 테스트 작성은 훨씬 더 쉽고 지루하지 않을 것이다.

## 마법
테스트의 인수 목록에 fixture 함수의 이름을 포함하여 테스트에 fixture를 주입한다.

```python
import pytest
from seuss.characters import ThingOne, ThingTwo
from seuss.comparisons import are_twins


@pytest.fixture
def thing_one():
    t1 = ThingOne()
    return t1

@pytest.fixture
def thing_two():
    t2 = ThingTwo()
    return t2

def test_book(thing_one, thing_two):
    assert are_twins(thing_one, thing_two)
```

(잘 모르시는 분은 Suess박사의 책 [The Cat In The Hat](https://amzn.to/3X2qcw7)를 참고하세요.)

여기서 일어나는 일은 pytest가 모든 테스트를 찾기 위해 코드베이스를 탐색하는 것이다. 이를 [test discovery](https://docs.pytest.org/en/7.1.x/explanation/goodpractices.html#conventions-for-python-test-discovery)이라고 한다. 이 과정에서 테스트 함수에 대한 인수 목록을 검사하고 이를 fixture와 일치시킨다.

그런 다음 종속성 트리를 기반으로 모든 fixture 생성 시간을 최소화하기 위해 테스트 실행을 구성한다. fixture 작성자는 fixture 데이터의 범위/라이프사이클을 제어할 수 있다. [pytest 픽스처 범위](https://betterprogramming.pub/understand-5-scopes-of-pytest-fixtures-1b607b5c19ed)에 자세한 설명을 읽을 수 있다.

이제 테스트의 인수 목록에 `bob`을 추가하여 `bob`이라는 이름의 fixture를 만들면 해당 fixture의 데이터를 얻을 수 있다는 것을 알았다. 따라서 수백 개의 테스트에서 fixture 데이터를 쉽게 공유할 수 있다.

## fixture의 구성 가능성
fixture를 각각 위에 만들 수 있다는 점을 인식하는 것이 중요하다.

Github 너티 이슈 댓글 예로 돌아가 보자. 다음과 같은 fixture를 갖도록 구조를 만들겠다.

- `owner`
- `public_repo`
- `private_repo`
- `public_issue`
- `private_issue`

등록 프로세스를 구축할 때 이미 등록된 사용자, 즉 리포지토리 소유자 역할을 할 수 있는 fixture가 있을 수 있다. 따라서 다음 두 fixture에 대해서는 해당 fixture를 인수로 사용하면 자동으로 가져올 수 있다.

```python
@pytest.fixture
def owner():
    return make_registred_user()

@pytest.fixture
def public_repo(owner):
    return make_repo(owner=owner, visibility="public")

@pytest.fixture
def private_repo(owner):
    return make_repo(owner=owner, visibility="private")
```

이제 앞으로 작성하는 모든 테스트에 대해 public 및/또는 private 리포지토리를 빠르게 확보할 수 있다. 이러한 수정 사항은 프로젝트의 모든 테스트에서 사용할 수 있다는 점을 기억하세요. 이러한 fixture는 Python 네임스페이스와 소스 파일 전체에 삽입된다. 따라서 테스트 파일 맨 위에 있는 수십 개의 항목을 임포트해야 하는 번거로움이 사라진다.

그러나 이는 또한 잠시 시간을 내서 fixture의 이름이 fixture를 잘 표현하고 기억하기 쉽고, 타이핑하기 쉽고, 철자가 틀리지 않는 좋은 이름인지 확인해야 한다는 것을 의미gks다. 자, 다시 작업을 시작하자.

우리의 임무는 공개 이슈 댓글에서 욕설이 다시 쓰여지도록 하는 것이다. 이러한 fixture는 도움이 되지만 우리가 원하는 목표의 절반만 달성할 수 있다. 더 많은 fixture를 추가하면 이 문제를 해결할 수 있다!

```python
from issues.utils import create_issue


@pytest.fixture
def public_issue(public_repo):
    return create_issue(repo=public_repo, title="Something is broken")

@pytest.fixture
def private_issue(private_repo):
    return create_issue(repo=private_repo, title="Something is broken in private")
```

이제 두 비트의 데이터와 함께 제공되는 데이터 트리가 모두 생성되었으므로 실제로 작업을 테스트할 수 있다. 다음과 같이 보일 수 있다.

```python
def test_naughty_comment_public(public_issue):
    comment = public_issue.new_comment("Fuck off asshole")
    assert comment.body === "F*** off a**hole"

def test_naughty_comment_private(private_issue):
    comment = private_issue.new_comment("Fuck off asshole")
    assert comment.body === "Fuck off asshole"
```

## 반환할 수 있는 항목
지금쯤이면 fixture로부터 모든 종류의 객체를 반환할 수 있다는 사실을 눈치챘을 것이다. 다음은 프로젝트에 어떻게 활용할 수 있을지 머릿속에 떠오르는 몇 가지 아이디어이다.

### 간단한 정적 데이터

```python
@pytest.fixture
def password():
    # We can return simple strings
    return "top-secret"

@pytest.fixture
def pi():
    # We can return special/constant values
    return 3.14159

@pytest.fixture
def default_preferences():
    # We can return structures of things that are commonly needed
    return {
        "default_repo_visibility": "public",
        "require_2fa": true,
        "naughty_comments_allowed": false,
    }
```

### 인스탄스화된 객체
인스턴스화하기 복잡하거나 처리 시간 측면에서 비용이 많이 드는 객체를 액세스해야 하는 경우가 종종 있다. 이러한 경우 인스턴스화 자체를 픽스처로 전환하여 여러 테스트에서 더 쉽게 재사용할 수 있다.

```python
from core.models import Organization

@pytest.fixture
def big_org():
    return Organization(members=1000)

@pytest.fixture
def small_org():
    return Organization(members=5)
```

### 다른 것을 만들기 위한 콜러블(Callables)
함수나 기타 콜러블을 반환하는 fixture를 사용하면 테스트에 쉽게 삽입할 수 있다.

```python
from core.utils import make_new_user


@pytest.fixture
def make_user():
    return make_new_user

def test_user_creation(make_user):
    new_user = make_new_user(username='frank')
    assert new_user.is_active
    assert new_user.username == 'frank'
```

또는 이러한 함수를 더 쉽게 사용할 수 있도록 커리하는 데 사용할 수도 있다.

```python
from core.utils import make_complicated_user


@pytest.fixture
def make_admin():
    def inner_function(username, email, password):
        return make_complicated_user(
            username=username,
            email=email,
            password=password,
            is_admin=True,
            can_create_public_repos=True,
            can_create_private_repos=True,
        )
    return inner_function

def test_admin_creation(make_admin):
    admin = make_admin(username='frank', email='frank@revsys.com', password='django')
    assert admin.username == 'frank'
    assert admin.is_admin == True
```

## fixture 정리
REVSYS는 주로 Django 프로젝트와 함께 작업하기 때문에 fixture는 해당 fixture에 사용되는 모델과 함께 Django 앱과 함께 작동하는 경향이 있다. 물론 반드시 이렇게 해야 하는 것은 아니며, Django 프로젝트의 맥락에서 의미가 있을 뿐이다.

테스트에서 여기저기서 fixture를 정의할 수 있다. 다만 어디에서 찾아야 할지 알기가 더 어려워질 뿐이다.

하나의 테스트 파일 외부에서 fixture를 재사용하지 않는다면 해당 파일에 보관하는 것이 절대적으로 적절하다. 프로젝트 전체에서 사용되는 모든 테스트는 중앙 또는 준중앙 위치에 있어야 한다.

물론 pytest가 이를 도와준다. pytest는 pytest를 실행하는 디렉터리(및 하위 디렉터리)에서 `conftest.py`라는 파일을 찾아 테스트 파일 자체 외부에 있는 fixture를 찾을 위치를 정의할 수 있다.

다음은 REVSYS의 일반적인 프로젝트 구조이다.

```
project-repo/ 
    config/ 
        settings.py 
        urls.py 
        wsgi.py 
    users/ 
        models.py 
        views.py 
        tests/ 
            fixtures.py 
            test_models.py 
            test_views.py 
    orgs/ 
        models.py 
        views.py 
        tests/ 
            fixtures.py 
            test_models.py 
            test_views.py
```

프로젝트에서 공유하려는 모든 fixture를 `tests/fixtures.py` 파일에 저장한다. 그런 다음 다음과 같이 `project-repo/conftest.py`를 정의한다.

```python
import pytest

from users.tests.fixtures import *
from orgs.tests.fixtures import *

@pytest.fixture
def globalthing():
    return "some-global-project-data-here"
```

보시다시피, 특정 Django 앱 디렉터리가 아닌 프로젝트 전체 또는 글로벌 fixture가 더 적합하다면 `conftest.py` 파일에서 직접 정의할 수도 있다.

## 플러그인 fixture
pytest 플러그인은 fiture를 생성하여 제공할 수도 있다. Django 사용자의 경우, [pytest-django](https://pytest-django.readthedocs.io/en/latest/)는 몇 가지 유용한 픽스처를 제공한다.

- `db`는 데이터베이스 구성이 필요한 것으로 테스트에 자동으로 표시한다.
- `client`는 `django.test.Client`의 인스턴스를 제공한다.
- `admin_user`는 자동으로 생성된 수퍼유저를 반환한다.
- `admin_client`는 이미 `admin_user`로 로그인한 테스트 클라이언트의 인스턴스를 반환한다.
- `settings`는 `django.conf.settings`를 제공한다.

다른 예를 보여주기 위해, 라이브러리 `django-test-plus`는 `tp`라는 이름을 사용하여 스스로를 pytest fixture로서 제공한다. 이렇게 하면 pytest 스타일 테스트에서 제공하는 테스트 지원 기능을 더 쉽게 활용할 수 있다. 다음은 간단한 예이다.

```python
def test_named_view(tp, admin_user):
    # GET the view named 'my-named-view' handling the reverse for you
    response = tp.get('my-named-view')
    # Ensure the response is a 401 because we need to be logged in
    tp.response_401(response)

    # Login as the admin_user and ensure a 200
    with tp.login(admin_user):
        response = tp.get('my-named-view')
        tp.response_200(response)
```

이 작업은 라이브러리에서 [config의 단지 몇 줄](https://github.com/revsys/django-test-plus/blob/main/setup.cfg#L46-L48)과 [fixture 자체](https://github.com/revsys/django-test-plus/blob/main/test_plus/plugin.py)만으로 수행된다. 즉, `pip`이 설치된 내부 라이브러리에도 fixture를 제공할 수 있으며 단일 리포지토리에만 fixture를 두는 것에 국한되지 않는다.

## 어디서나 자동으로 pytest fixture 사용
지금까지 살펴본 모든 것이 훌륭하지만, 대부분의 코드 베이스에서 수많은 fixture가 필요한 코드 베이스에 직면할 수도 있다. 또는 대부분의 코드 베이스에 필요한 fixture가 몇 개뿐일 수도 있다. pytest가 도와주니 걱정하지 마세요.

각 테스트에 개별적으로 인자로 지정하지 않고도 모든 테스트에 대해 자동으로 실행되도록 fixture를 정의할 수 있다.

```python
@pytest.fixture(autouse=True)
def global_thing():
    # Create something in our database we need everywhere or always
    Config.objects.create(something=True)

def test_something_lame():
    assert Config.objects.filter(something=True).exists()
```

## 내재 fixture
pytest는 다양한 내장 기능을 제공한다. 이 중 몇 가지 기능은 매우 유용하게 사될 수 있다.

- `tmpdir`은 각 테스트에 고유한 임시 디렉토리 경로 객체를 반환한다.
- `tmp_path`는 [Path](https://docs.python.org/3/library/pathlib.html#basic-use) 객체로 임시 디렉터리를 제공한다.
- `stdout`과 `stderr`을 캡처하기 위한 `capsys` fixture
- 로깅 출력을 캡처하는 `caplog` fixture
- `monkeypatch`는 객체, 딕셔너리 등을 빠르고 쉽게 몽키 패치한다.
- 테스트 세션 사이에 상태를 유지할 수 있는 캐시 객체를 `cache`한다.

## 추가 학습
자세한 내용은 [pytest fixture 문서](https://docs.pytest.org/en/7.3.x/how-to/fixtures.html)에서 알아보거나  [내장 픽스처의 전체 목록](https://docs.pytest.org/en/7.3.x/builtin.html)을 살펴보세요.

## 마치며
의미 있는 테스트로 커버되는 코드베이스가 많을수록 새로운 기능을 더 빨리 개발하고 결함을 리팩터링할 수 있다. 테스트는 단순히 버그가 없는 코드를 넘어 좋은 코드를 개발할 수 있는 속도에 관한 것이다.

테스트를 더 쉽게 작성할수록 훌륭한 소프트웨어를 더 빨리 개발할 수 있다.

# pytest fixtures are magic <sup>[1](#footnote_1)</sup>

저자는 보통 코드의 마법을 싫어하지만 이 경우는 환상적이다!

저자는 pytest 스타일 테스트를 작성하고 사용하기 전에는 부끄러울 정도로 오랫동안 테스트를 실행하는 데 [pytest](https://pytest.org/)를 사용했다는 사실을 인정해야 한다. 게으름과 pytest의 마법을 이해하지 못했기 때문이라고 생각한다.

어떤 마법이 일어나는지 이해하지 못했기 때문에 pytest를 더 깊이 들여다보는 것이 두려웠다. 한 번은 누군가 저자를 붙잡고 목구멍으로 설명을 강요했으면 좋겠다고 생각했다.

fixture는 좋은 테스트를 작성하기 위한 기본 요소이다. 훌륭한 fixture가 있으면 훌륭한 테스트를 작성하기가 쉬워지고 개발 속도도 빨라진다.

<a name="footnote_1">1</a>: 이 페이지는 [pytest fixtures are magic](https://medium.com/@fwiles/pytest-fixtures-are-magic-fe25549e3b85)를 편역한 것이다.
